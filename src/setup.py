from setuptools import setup, find_packages

setup(
    name='onclass',
    description='This code should be in the console... I worked from home',
    requires=['typing', 'pytest'],
    version='1.0.0',
    packages=find_packages(),
)
