from onclass.console.devices.devices import Plugin

"""
TODO: Targets:
    0. The code works
    1. Add @on(AmericanDevice, BritishDevice)
        a. Implement overloading for both functions and classes
        b. make sure all edge cases are ok (tests) - @on function must be a subset of @on class
    2. Make FlightPlugin understand that 'visa' exists for American & BritishDevice
"""

@on(AmericanDevice, BritishDevice)
class FlightPlugin(Plugin):
    def book_flight(self):
        visa = self.asset.visa.get_visa()
        return visa == 'Yes'

    @on(DutchDevice)   ## exception or other behaviour to be decided
    def book_dutch_flight(self):
        pass