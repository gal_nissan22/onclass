class Device(object):
    pass


class AmericanDevice(Device):
    pass


class DutchDevice(Device):
    pass


class BritishDevice(Device):
    pass


class Plugin(object):
    __device__ = Device


class AmericanPlugin(Plugin):
    __device__ = AmericanDevice


class DutchPlugin(Plugin):
    __device__ = DutchDevice


class BritishPlugin(Plugin):
    __device__ = BritishDevice

