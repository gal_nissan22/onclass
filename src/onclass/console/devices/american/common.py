from onclass.console.assets.asset import Asset
from onclass.console.devices.devices import AmericanPlugin
from onclass.console.plugins.visa import VisaPlugin


class AmericanCommon(Asset, AmericanPlugin):
    def __init__(self, asset):
        # type: (Asset[AmericanPlugin]) -> None
        super(AmericanCommon, self).__init__(asset)
        self.visa = VisaPlugin()
