from onclass.console.assets.asset import Asset
from onclass.console.devices.devices import BritishPlugin


class BritishCommon(Asset, BritishPlugin):
    ASSET_NAME = 'British'
