from typing import Generic, TypeVar

T = TypeVar('T')


class Asset(Generic[T]):
    ASSET_NAME = 'Asset'

    def __init__(self, asset):
        # type: (object) -> None
        self.asset = asset
