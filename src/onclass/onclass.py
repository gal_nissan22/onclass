from typing import TypeVar, Callable, Any, Type, Union, overload, cast, Dict

from console.devices.devices import Device, AmericanDevice, BritishDevice

T_Func = TypeVar('T_Func', bound=Callable[..., Any])
T_Prop = property
T_Item = TypeVar('T_Item', bound=Union[Callable[..., Any], property])
T_Device = Type[Device]
T_Class = TypeVar('T_Class')

IMPLEMENTATIONS_MAP_KEY = 'implementations-map'
ON_CLASS_IGNORE = \
    ['__init__']  # '__dict__', '__module__', '__weakref__', '__doc__' ignored too (they're not  methods or properties)


class OnWrapper(object):
    def __init__(self, *devices):
        # type: (*T_Device) -> None
        for device in devices:
            assert issubclass(device, Device) and device is not Device
        self.devices = devices

    def _wrap_function(self, func):
        # type: (T_Func) -> None
        if not func:
            return
        func_implementations_map = {}
        for device in self.devices:
            func_implementations_map[device] = func
        setattr(func, IMPLEMENTATIONS_MAP_KEY, func_implementations_map)

    @overload
    def wrap(self, original_item):
        # type: (T_Prop) -> T_Prop
        pass

    @overload
    def wrap(self, original_item):
        # type: (T_Func) -> T_Func
        pass

    def wrap(self, original_item):
        # type: (Union[T_Prop, T_Func]) -> Union[T_Prop, T_Func]
        if type(original_item) is property:
            for func in [original_item.fget, original_item.fset, original_item.fdel]:
                self._wrap_function(cast(T_Func, func))
        else:
            self._wrap_function(cast(T_Func, original_item))
        return original_item

    @staticmethod
    def is_wrapped(item):
        # type: (Union[T_Prop, T_Func]) -> bool
        if type(item) is property:
            for func in [item.fget, item.fset, item.fdel]:
                if func and not hasattr(func, IMPLEMENTATIONS_MAP_KEY):
                    return False
            return True
        else:
            return hasattr(item, IMPLEMENTATIONS_MAP_KEY)


def on(*devices):
    # type: (*T_Device) -> Callable[[T_Item], T_Item]
    """
    Should behave like the original @on:
        * Accept devices as *args
        * set function parameter that holds the mapping-dict
        - should not actually support the overloading because when the @on_class accepts a class-
            there is one method with a dict
    :param devices: *args type should be subclasses of Device
    :return: wrapped function
    """
    wrapper = OnWrapper(*devices)
    return wrapper.wrap


def get_relevant_class_items(input_class):
    # type: (T_Class) -> Dict[str, Union[T_Func, T_Prop]]
    properties = filter(
        lambda (key, value): key not in ON_CLASS_IGNORE and type(value) is property
        and not OnWrapper.is_wrapped(value),
        input_class.__dict__.items()
    )
    methods = filter(
        lambda (key, value): key not in ON_CLASS_IGNORE and hasattr(value, '__call__')
        and not OnWrapper.is_wrapped(value),
        input_class.__dict__.items()
    )
    return dict(properties + methods)


def on_class(*devices):
    # type: (*T_Device) -> Callable[[T_Class], T_Class]
    def class_wrapper(input_class):
        # type: (T_Class) -> T_Class
        for item in get_relevant_class_items(input_class).values():
            on(*devices)(item)
        return input_class
    return class_wrapper


DEVICES = [AmericanDevice, BritishDevice]
