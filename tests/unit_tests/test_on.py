import pytest

from onclass.onclass import IMPLEMENTATIONS_MAP_KEY, on, OnWrapper
from onclass.console.devices.devices import DutchDevice, BritishDevice, AmericanDevice, Device


DEVICES = [DutchDevice, BritishDevice, AmericanDevice]


def func_without_on():
    pass


@on(*DEVICES)       # equivalent to @on(DutchDevice, BritishDevice, AmericanDevice)
def func_with_on():
    pass


class TestClass(object):
    @property
    def prop(self):
        return 1

    @prop.setter
    def prop(self, value):
        pass

    prop = on(*DEVICES)(prop)

    @on(*DEVICES)
    def method(self):
        pass

    @on(*DEVICES)
    def _private_method(self):
        pass

    @on(*DEVICES)
    def __call__(self, *args, **kwargs):
        # overridden method
        pass

    @staticmethod
    @on(*DEVICES)
    def static_method(self):
        pass

    @classmethod
    @on(*DEVICES)
    def class_method(cls):
        pass


def test_function_without_on():
    assert not hasattr(func_without_on, IMPLEMENTATIONS_MAP_KEY)


@pytest.mark.parametrize(
    'func', [
        func_with_on, TestClass.method, TestClass._private_method, TestClass.__call__, TestClass.static_method,
        TestClass.class_method
    ]
)
def test_function_with_on_decorator(func):
    # note that more than single-on implementation won't work because our demo for @on isn't support it.
    assert hasattr(func, IMPLEMENTATIONS_MAP_KEY)
    implementations = getattr(func, IMPLEMENTATIONS_MAP_KEY)
    assert sorted(implementations.keys()) == sorted(DEVICES)


def test_on_property():
    prop = TestClass.prop
    assert type(prop is property) and not hasattr(prop, IMPLEMENTATIONS_MAP_KEY)
    assert sorted(getattr(prop.fget, IMPLEMENTATIONS_MAP_KEY, {}).keys()) == sorted(DEVICES)
    assert sorted(getattr(prop.fset, IMPLEMENTATIONS_MAP_KEY, {}).keys()) == sorted(DEVICES)


@pytest.mark.parametrize(
    'failure_type', [Device, int, str]
)
def test_on_with__device__class(failure_type):
    with pytest.raises(AssertionError):
        @on(failure_type)
        def foo():
            pass


@pytest.mark.parametrize(
    'item', [
        func_with_on, TestClass.method, TestClass._private_method, TestClass.__call__, TestClass.static_method,
        TestClass.class_method, TestClass.prop
    ]
)
def test__on_wrapper__wrapped_method(item):
    assert OnWrapper.is_wrapped(item)
