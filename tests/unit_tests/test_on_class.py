import pytest

from onclass.onclass import on, get_relevant_class_items, on_class
from onclass.console.devices.devices import DutchDevice, BritishDevice, AmericanDevice


DEVICES = [DutchDevice, BritishDevice, AmericanDevice]


@pytest.fixture
def test_class():
    class TestClass(object):
        def __init__(self):
            pass

        @property
        def prop(self):
            return 1

        @prop.setter
        def prop(self, value):
            pass

        @property
        def on_prop(self):
            return 1

        on_prop = on(*DEVICES)(on_prop)

        def method(self):
            pass

        @on(*DEVICES)
        def on_method(self):
            pass

        def _private_method(self):
            pass

        @on(*DEVICES)
        def _on_private_method(self):
            pass

        def __eq__(self, other):
            return True

        @on(*DEVICES)
        def __call__(self, *args, **kwargs):
            # overridden on-method
            pass

        @staticmethod
        def static_method():
            pass

        @staticmethod
        @on(*DEVICES)
        def on_static_method():
            pass

        @classmethod
        def class_method(cls):
            pass

        @classmethod
        @on(*DEVICES)
        def on_class_method(cls):
            pass
    return TestClass


def test_get_relevant_class_items(test_class):
    relevant_items_names = ['prop', 'method', '_private_method', '__eq__']  # , 'static_method', 'class_method']
    relevant_items = get_relevant_class_items(test_class)
    assert sorted(relevant_items.keys()) == sorted(relevant_items_names)

